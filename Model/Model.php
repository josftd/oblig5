<?php

class Model{
protected $db = null;
Public $do;

	public function __construct()  
	{  

	$this->do = new DOMDocument();
	$this->do->load('Model/SkierLogs.xml');

	try{ 
	$this->db = new PDO('mysql:host=localhost;dbname=oblig5; charset=utf8', 'root','');
	}catch(PDOException $e){
		echo "error";
		$e->getMessage();
	}
	}

	public function main(){
		$this->resclub();
		$this->resskier();
		$this->resseason();
		$this->resdistance_skier();
		
	}

	public function resclub(){
		$club = array();
		$cName = array();
		$city = array();
		$cty = array();
		
		$Club = $this->do->getElementsByTagName('Club');
		$Name = $this->do->getElementsByTagName('Name');
		$citys = $this->do->getElementsByTagName('City');
		$countys = $this->do->getElementsByTagName('County');
		
		foreach($Club as $cb){
			array_push($club, $cb->getAttribute('id'));
		}
		
		foreach($Name as $navn){
			array_push($cName, $navn->textContent);
		}
		
		foreach($citys as $by){
			array_push($city, $by->textContent);
		}
		
		foreach($countys as $fylke){
			array_push($cty, $fylke->textContent);
		}
		

		for($i = 0; $i <sizeof($club); $i++){
			try{ 
				$req = $this->db->prepare("INSERT INTO club (clubId, clubName, city, county) VALUES (:cid, :cn, :by, :c)");
				$req->bindValue(':cid',$club[$i], PDO::PARAM_STR);
				$req->bindValue(':cn',$cName[$i], PDO::PARAM_STR);
				$req->bindValue(':by',$city[$i], PDO::PARAM_STR);
				$req->bindValue(':c', $cty[$i], PDO::PARAM_STR);
				$req->execute(); 
			}
            catch(PDOException $e){
                echo "error!";
                $e->getMessage();
            }
		}
	
	 
	}
	
	public function resskier(){
		$uName = array();
		$fName = array();
		$lName = array();
		$yob = array();
		
		$userName = $this->do->getElementsByTagName('Skier');
		$firstName = $this->do->getElementsByTagName('FirstName');
		$lastName = $this->do->getElementsByTagName('LastName');
		$yofb = $this->do->getElementsByTagName('YearOfBirth');
		
		foreach($userName as $un){
			array_push($uName, $un->getAttribute('userName'));
		}
		
		foreach($firstName as $fn){
			array_push($fName, $fn->textContent);
		}
		
		foreach($lastName as $ln){
			array_push($lName, $ln->textContent);
		}
		
		foreach($yofb as $yb){
			array_push($yob, $yb->textContent);
		}
		
		
		
		for($i = 0; $i <sizeof($userName); $i++){

			try{ 

				$req = $this->db->prepare("INSERT INTO cskier (userName, firstName, lastName, yearOfBirth) VALUES (:u, :f, :l, :y)");
                $req->bindValue(':u',$uName[$i], PDO::PARAM_STR);
                $req->bindValue(':f',$fName[$i], PDO::PARAM_STR);
                $req->bindValue(':l',$lName[$i], PDO::PARAM_STR);
                $req->bindValue(':y',$yob[$i], PDO::PARAM_STR);
                $req->execute(); 

			}

            catch(PDOException $e){
                echo "error";
                $e->getMessage();
            }
		}
	    
	 
	}
	
	public function resseason(){
		$Season = array();
		
		$Seas = $this->do->getElementsByTagName('Season');
		
		foreach($Seas as $ses){
			array_push($Season, $ses->getAttribute('fallYear'));
		}
		

		
		
		for($i = 0; $i <sizeof($Season); $i++){
			try{ 
				$req = $this->db->prepare("INSERT INTO season (fallYear) VALUES (:f)");
				$req->bindValue(':f',$Season[$i], PDO::PARAM_STR);
				$req->execute(); 
			}
            catch(PDOException $e){
                echo "error";
                $e->getMessage();
            }
		}
	    
	 
	}
	
	public function resdistance_skier(){
		$uName15 = array();
		$uName16 = array();
		$d15 = array();
		$d16 = array();
		
		$path = new DOMXPath($this->do);
		
		$u15 = $path->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier/@userName');
		
		foreach($u15 as $us15){
			array_push($uName15, trim($us15->textContent));
		}
		
		$sum = 0;
		
		for($i = 0; $i < sizeof($uName15); $i++){
			$dist1 = $path->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$uName15[$i].'"]/Log/Entry/Distance');
			for($j = 0; $j < $dist1->length; $j++){
				$sum += ($dist1->item($j)->textContent);
			}
			array_push($d15, $sum);
			$sum = 0;
		}
		
		$u16 = $path->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier/@userName');
		
		foreach($u16 as $us16){
			array_push($uName16, trim($us16->textContent));
		}
		
		$sum = 0;
		
		for($i = 0; $i < sizeof($uName16); $i++){
			$dist2 = $path->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$uName16[$i].'"]/Log/Entry/Distance');
			for($j = 0; $j < $dist2->length; $j++){
				$sum += ($dist2->item($j)->textContent);
			}
			array_push($d16, $sum);
			$sum = 0;
		}
		
		$se = $path->query('//SkierLogs/Season/@fallYear');

        for($i = 0; $i< sizeof($uName15); $i++){
            try {
                $null = NULL;
                $club = $path->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$uName15[$i].'"]/ancestor::Skiers/@clubId');
                $req = $this->db->prepare("INSERT INTO distance(userName, fallYear, clubId, totalDistance) VALUES (:u, :f, :c, :total)");
                $req->bindValue(':u', $uName15[$i], PDO::PARAM_STR);
                $req->bindValue(':f', $se->item(0)->textContent, PDO::PARAM_STR);
                $req->bindValue(':total', $d15[$i], PDO::PARAM_STR);
                if($club->length){
                    $req->bindValue(':c', $club->item(0)->textContent, PDO::PARAM_STR);
                }else $req->bindValue(':c', $null, PDO::PARAM_STR);
                $req->execute();
            } catch (Exception $e) {
                echo "error";
                echo $e->getMessage();
            }

        }
		
		for($i = 0; $i< sizeof($uName16); $i++){
            try {
                $null = NULL;
                $club = $path->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$uName16[$i].'"]/ancestor::Skiers/@clubId');
                $req = $this->db->prepare("INSERT INTO distance(userName, fallYear, clubId, totalDistance) VALUES (:u, :f, :c, :total)");
                $req->bindValue(':u', $uName16[$i], PDO::PARAM_STR);
                $req->bindValue(':f', $se->item(1)->textContent, PDO::PARAM_STR);
                $req->bindValue(':total', $d16[$i], PDO::PARAM_STR);
                if($club->length){
                    $req->bindValue(':c', $club->item(0)->textContent, PDO::PARAM_STR);
                }else $req->bindValue(':c', $null, PDO::PARAM_STR);
                $req->execute();
            } catch (Exception $e) {
                echo "error";
                echo $e->getMessage();
            }

        }	
			
	}
	
}  

?>
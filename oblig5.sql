-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05. Nov, 2017 22:05 PM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oblig5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `clubId` varchar(30) NOT NULL,
  `clubName` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `club`
--

INSERT INTO `club` (`clubId`, `clubName`, `city`, `county`) VALUES
('asker-ski', 'Asker skiklubb', 'Asker', 'Akershus'),
('lhmr-ski', 'Lillehammer Skiklub', 'Lillehammer', 'Oppland'),
('skiklubben', 'Trondhjems skiklub', 'Trondheim', 'Sør-Trøndelag'),
('vindil', 'Vind Idrettslag', 'Gjøvik', 'Oppland');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `cskier`
--

CREATE TABLE `cskier` (
  `userName` varchar(60) NOT NULL,
  `firstName` varchar(60) NOT NULL,
  `lastName` varchar(60) NOT NULL,
  `yearOfBirth` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `cskier`
--

INSERT INTO `cskier` (`userName`, `firstName`, `lastName`, `yearOfBirth`) VALUES
('ande_andr', 'Anders', 'Andresen', '2004');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `distance`
--

CREATE TABLE `distance` (
  `userName` varchar(50) NOT NULL,
  `fallYear` varchar(4) NOT NULL,
  `clubId` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `totalDistance` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `distance`
--

INSERT INTO `distance` (`userName`, `fallYear`, `clubId`, `totalDistance`) VALUES
('?hal_?mos', '2016', 'asker-ski', '3'),
('?rut_?mos', '2016', 'vindil', '1237'),
('?rut_nord', '2016', 'skiklubben', '368'),
('ande_andr', '2015', 'skiklubben', '23'),
('ande_andr', '2016', 'skiklubben', '55'),
('ande_rønn', '2015', 'lhmr-ski', '942'),
('andr_stee', '2015', 'asker-ski', '440'),
('andr_stee', '2016', 'asker-ski', '379'),
('anna_næss', '2015', 'skiklubben', '3'),
('anna_næss', '2016', 'skiklubben', '3'),
('arne_anto', '2015', 'skiklubben', '32'),
('arne_anto', '2016', 'skiklubben', '99'),
('arne_inge', '2015', 'skiklubben', '1'),
('arne_inge', '2016', 'skiklubben', '2'),
('astr_amun', '2015', 'lhmr-ski', '961'),
('astr_amun', '2016', 'lhmr-ski', '761'),
('astr_sven', '2015', 'skiklubben', '1'),
('astr_sven', '2016', 'skiklubben', '3'),
('bent_håla', '2015', 'asker-ski', '19'),
('bent_håla', '2016', 'skiklubben', '62'),
('bent_svee', '2015', 'asker-ski', '125'),
('beri_hans', '2015', 'asker-ski', '448'),
('beri_hans', '2016', 'asker-ski', '374'),
('bjør_aase', '2015', 'asker-ski', '121'),
('bjør_aase', '2016', 'asker-ski', '116'),
('bjør_ali', '2015', 'asker-ski', '47'),
('bjør_ali', '2016', 'asker-ski', '47'),
('bjør_rønn', '2015', 'lhmr-ski', '33'),
('bjør_rønn', '2016', 'lhmr-ski', '56'),
('bjør_sand', '2015', 'lhmr-ski', '460'),
('bjør_sand', '2016', 'lhmr-ski', '449'),
('bror_?mos', '2016', 'skiklubben', '243'),
('cami_erik', '2015', 'vindil', '1'),
('cami_erik', '2016', 'vindil', '1'),
('dani_hamm', '2015', 'lhmr-ski', '33'),
('dani_hamm', '2016', 'lhmr-ski', '61'),
('eina_nygå', '2015', 'lhmr-ski', '31'),
('eina_nygå', '2016', 'skiklubben', '68'),
('elis_ruud', '2015', 'asker-ski', '341'),
('elis_ruud', '2016', 'skiklubben', '368'),
('elle_wiik', '2015', 'lhmr-ski', '12'),
('elle_wiik', '2016', 'lhmr-ski', '35'),
('erik_haal', '2015', 'lhmr-ski', '122'),
('erik_haal', '2016', 'lhmr-ski', '143'),
('erik_lien', '2015', 'vindil', '1'),
('erik_pete', '2015', 'vindil', '581'),
('espe_egel', '2015', 'skiklubben', '519'),
('espe_egel', '2016', 'skiklubben', '556'),
('espe_haal', '2015', 'lhmr-ski', '1'),
('espe_haal', '2016', 'lhmr-ski', '2'),
('eva_kvam', '2015', 'skiklubben', '28'),
('eva_kvam', '2016', 'skiklubben', '89'),
('fred_lien', '2015', 'asker-ski', '113'),
('fred_lien', '2016', 'asker-ski', '122'),
('frod_mads', '2015', 'skiklubben', '1'),
('frod_mads', '2016', 'skiklubben', '2'),
('frod_rønn', '2015', 'lhmr-ski', '237'),
('geir_birk', '2015', 'skiklubben', '69'),
('geir_birk', '2016', 'skiklubben', '71'),
('geir_herm', '2015', 'asker-ski', '891'),
('geir_herm', '2016', 'skiklubben', '789'),
('gerd_svee', '2015', 'lhmr-ski', '173'),
('gerd_svee', '2016', 'lhmr-ski', '196'),
('gunn_berg', '2015', 'vindil', '2'),
('gunn_berg', '2016', 'vindil', '2'),
('guri_nord', '2016', 'skiklubben', '17'),
('hann_stei', '2016', 'lhmr-ski', '14'),
('hans_foss', '2015', 'asker-ski', '240'),
('hans_foss', '2016', 'lhmr-ski', '276'),
('hans_løke', '2015', 'skiklubben', '3'),
('hans_løke', '2016', 'skiklubben', '1'),
('hara_bakk', '2015', 'lhmr-ski', '7'),
('hara_bakk', '2016', 'lhmr-ski', '16'),
('heid_dani', '2015', 'asker-ski', '3'),
('heid_dani', '2016', 'asker-ski', '3'),
('helg_brei', '2015', 'skiklubben', '27'),
('helg_brei', '2016', 'skiklubben', '74'),
('helg_toll', '2015', 'skiklubben', '9'),
('henr_bern', '2015', 'asker-ski', '799'),
('henr_lore', '2015', 'vindil', '1'),
('henr_lore', '2016', 'vindil', '1'),
('hild_hass', '2015', 'lhmr-ski', '2'),
('hild_hass', '2016', 'lhmr-ski', '1'),
('håko_jens', '2015', 'lhmr-ski', '778'),
('håko_jens', '2016', 'lhmr-ski', '804'),
('idar_kals', '2016', 'skiklubben', '101'),
('idar_kals1', '2016', 'vindil', '1308'),
('ida_mykl', '2015', 'skiklubben', '666'),
('ida_mykl', '2016', 'skiklubben', '614'),
('inge_simo', '2015', 'asker-ski', '3'),
('inge_simo', '2016', 'asker-ski', '2'),
('ingr_edva', '2015', 'skiklubben', '294'),
('ingr_edva', '2016', 'skiklubben', '309'),
('juli_ande', '2015', 'skiklubben', '20'),
('juli_ande', '2016', 'skiklubben', '34'),
('kari_thor', '2015', 'skiklubben', '261'),
('kari_thor', '2016', 'skiklubben', '233'),
('kjel_fjel', '2015', 'asker-ski', '1'),
('kjel_fjel', '2016', 'skiklubben', '2'),
('knut_bye', '2015', 'lhmr-ski', '2'),
('kris_even', '2015', 'skiklubben', '586'),
('kris_hass', '2015', 'skiklubben', '4'),
('kris_hass', '2016', 'skiklubben', '11'),
('kris_hass1', '2015', 'skiklubben', '391'),
('kris_hass1', '2016', 'lhmr-ski', '334'),
('lind_lore', '2015', 'lhmr-ski', '578'),
('lind_lore', '2016', 'lhmr-ski', '551'),
('liv_khan', '2015', 'asker-ski', '178'),
('liv_khan', '2016', 'asker-ski', '183'),
('magn_sand', '2015', 'asker-ski', '200'),
('magn_sand', '2016', 'asker-ski', '166'),
('mari_dahl', '2015', 'lhmr-ski', '576'),
('mari_dahl', '2016', 'lhmr-ski', '492'),
('mari_eile', '2015', 'lhmr-ski', '18'),
('mari_eile', '2016', 'lhmr-ski', '18'),
('mari_stra', '2015', 'skiklubben', '41'),
('mari_stra', '2016', 'skiklubben', '35'),
('mart_halv', '2015', 'vindil', '63'),
('mart_halv', '2016', 'vindil', '50'),
('mona_lie', '2015', 'skiklubben', '7'),
('mona_lie', '2016', 'skiklubben', '12'),
('mort_iver', '2015', 'skiklubben', '2'),
('mort_iver', '2016', 'skiklubben', '4'),
('nils_bakk', '2015', 'lhmr-ski', '36'),
('nils_bakk', '2016', 'lhmr-ski', '93'),
('nils_knud', '2015', 'skiklubben', '4'),
('nils_knud', '2016', 'skiklubben', '2'),
('odd_moha', '2015', 'skiklubben', '352'),
('olav_eike', '2015', 'lhmr-ski', '2'),
('olav_eike', '2016', 'lhmr-ski', '2'),
('olav_hell', '2015', 'skiklubben', '1'),
('olav_lien', '2015', 'asker-ski', '408'),
('olav_lien', '2016', 'asker-ski', '423'),
('ole_borg', '2015', 'lhmr-ski', '311'),
('ole_borg', '2016', 'lhmr-ski', '314'),
('reid_hamr', '2015', 'skiklubben', '2'),
('reid_hamr', '2016', 'skiklubben', '3'),
('rolf_wiik', '2015', 'skiklubben', '749'),
('rolf_wiik', '2016', 'skiklubben', '632'),
('rune_haga', '2015', 'asker-ski', '228'),
('rune_haga', '2016', 'asker-ski', '248'),
('sara_okst', '2016', 'asker-ski', '5'),
('silj_mykl', '2015', 'asker-ski', '1'),
('silj_mykl', '2016', 'asker-ski', '2'),
('sive_nord', '2016', 'skiklubben', '1'),
('solv_solb', '2016', 'asker-ski', '1'),
('stia_andr', '2016', 'vindil', '9'),
('stia_haug', '2015', 'skiklubben', '412'),
('stia_haug', '2016', 'skiklubben', '443'),
('stia_henr', '2015', 'vindil', '62'),
('stia_henr', '2016', 'vindil', '49'),
('terj_mort', '2015', 'skiklubben', '119'),
('terj_mort', '2016', 'skiklubben', '95'),
('thom_inge', '2015', 'vindil', '15'),
('thom_inge', '2016', 'vindil', '26'),
('tom_bråt', '2015', 'vindil', '1'),
('tom_bråt', '2016', 'vindil', '1'),
('tom_bøe', '2015', 'vindil', '176'),
('tom_bøe', '2016', 'vindil', '194'),
('tom_jako', '2015', 'asker-ski', '18'),
('tom_jako', '2016', 'skiklubben', '33'),
('tore_gulb', '2015', 'lhmr-ski', '375'),
('tore_gulb', '2016', 'lhmr-ski', '342'),
('tore_svee', '2015', 'skiklubben', '1156'),
('tor_dale', '2015', 'skiklubben', '408'),
('tove_moe', '2015', 'asker-ski', '321'),
('tove_moe', '2016', 'asker-ski', '352'),
('trin_kals', '2016', 'lhmr-ski', '22'),
('tron_kris', '2015', 'skiklubben', '3'),
('tron_kris', '2016', 'skiklubben', '5'),
('tron_moen', '2015', 'vindil', '8'),
('tron_moen', '2016', 'vindil', '17'),
('øyst_aase', '2015', 'skiklubben', '2'),
('øyst_aase', '2016', 'skiklubben', '1'),
('øyst_lore', '2015', 'skiklubben', '13'),
('øyst_lore', '2016', 'skiklubben', '47'),
('øyst_sæth', '2015', 'vindil', '831'),
('øyst_sæth', '2016', 'vindil', '631'),
('øyvi_hell', '2015', 'asker-ski', '950'),
('øyvi_hell', '2016', 'asker-ski', '869'),
('øyvi_jens', '2015', 'skiklubben', '3'),
('øyvi_jens', '2016', 'skiklubben', '2'),
('øyvi_kvam', '2015', 'asker-ski', '18'),
('øyvi_vike', '2015', 'asker-ski', '20'),
('øyvi_vike', '2016', 'asker-ski', '52');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `season`
--

CREATE TABLE `season` (
  `fallYear` varchar(4) NOT NULL,
  `totalDistance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `season`
--

INSERT INTO `season` (`fallYear`, `totalDistance`) VALUES
('2015', 0),
('2016', 0);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(60) NOT NULL,
  `firstName` varchar(60) NOT NULL,
  `lastName` int(11) NOT NULL,
  `yearOfBirth` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `skier`
--

INSERT INTO `skier` (`userName`, `firstName`, `lastName`, `yearOfBirth`) VALUES
('?hal_?mos', '?Halvor', 0, '2009'),
('?jan_tang', '?Jan', 0, '2007'),
('?rut_?mos', '?Ruth', 0, '2002'),
('?rut_nord', '?Ruth', 0, '2006'),
('ande_andr', 'Anders', 0, '2004'),
('ande_rønn', 'Anders', 0, '2001'),
('andr_stee', 'Andreas', 0, '2001'),
('anna_næss', 'Anna', 0, '2005'),
('arne_anto', 'Arne', 0, '2005'),
('arne_inge', 'Arne', 0, '2005'),
('astr_amun', 'Astrid', 0, '2001'),
('astr_sven', 'Astrid', 0, '2008'),
('bent_håla', 'Bente', 0, '2009'),
('bent_svee', 'Bente', 0, '2003'),
('beri_hans', 'Berit', 0, '2003'),
('bjør_aase', 'Bjørn', 0, '2006'),
('bjør_ali', 'Bjørn', 0, '2008'),
('bjør_rønn', 'Bjørg', 0, '2009'),
('bjør_sand', 'Bjørn', 0, '1997'),
('bror_?mos', 'Bror', 0, '2005'),
('bror_kals', 'Bror', 0, '2006'),
('cami_erik', 'Camilla', 0, '2005'),
('dani_hamm', 'Daniel', 0, '2000'),
('eina_nygå', 'Einar', 0, '2009'),
('elis_ruud', 'Elisabeth', 0, '2003'),
('elle_wiik', 'Ellen', 0, '2004'),
('erik_haal', 'Erik', 0, '2007'),
('erik_lien', 'Erik', 0, '2008'),
('erik_pete', 'Erik', 0, '2002'),
('espe_egel', 'Espen', 0, '2005'),
('espe_haal', 'Espen', 0, '2004'),
('eva_kvam', 'Eva', 0, '2000'),
('fred_lien', 'Fredrik', 0, '2000'),
('frod_mads', 'Frode', 0, '2008'),
('frod_rønn', 'Frode', 0, '2005'),
('geir_birk', 'Geir', 0, '2010'),
('geir_herm', 'Geir', 0, '2003'),
('gerd_svee', 'Gerd', 0, '2001'),
('gunn_berg', 'Gunnar', 0, '2009'),
('guri_nord', 'Guri', 0, '2003'),
('hann_stei', 'Hanno', 0, '2005'),
('hans_foss', 'Hans', 0, '1998'),
('hans_løke', 'Hans', 0, '2005'),
('hara_bakk', 'Harald', 0, '2002'),
('heid_dani', 'Heidi', 0, '2005'),
('helg_brei', 'Helge', 0, '2006'),
('helg_toll', 'Helge', 0, '2003'),
('henr_bern', 'Henrik', 0, '2003'),
('henr_dale', 'Henrik', 0, '2005'),
('henr_lore', 'Henrik', 0, '2006'),
('hild_hass', 'Hilde', 0, '2007'),
('håko_jens', 'Håkon', 0, '2005'),
('idar_kals', 'Idar', 0, '2007'),
('idar_kals1', 'Idar', 0, '2002'),
('ida_mykl', 'Ida', 0, '2001'),
('inge_simo', 'Inger', 0, '2004'),
('inge_thor', 'Inger', 0, '2006'),
('ingr_edva', 'Ingrid', 0, '2001'),
('juli_ande', 'Julie', 0, '2003'),
('kari_thor', 'Karin', 0, '2002'),
('kjel_fjel', 'Kjell', 0, '2004'),
('knut_bye', 'Knut', 0, '2006'),
('kris_even', 'Kristian', 0, '2004'),
('kris_hass', 'Kristin', 0, '2003'),
('kris_hass1', 'Kristian', 0, '2004'),
('lind_lore', 'Linda', 0, '2004'),
('liv_khan', 'Liv', 0, '2006'),
('magn_sand', 'Magnus', 0, '2003'),
('mari_bye', 'Marit', 0, '2003'),
('mari_dahl', 'Marit', 0, '2004'),
('mari_eile', 'Marius', 0, '2000'),
('mari_stra', 'Marius', 0, '2005'),
('mart_halv', 'Martin', 0, '2002'),
('mona_lie', 'Mona', 0, '2004'),
('mort_iver', 'Morten', 0, '2003'),
('nils_bakk', 'Nils', 0, '2003'),
('nils_knud', 'Nils', 0, '2006'),
('odd_moha', 'Odd', 0, '2005'),
('olav_bråt', 'Olav', 0, '2000'),
('olav_eike', 'Olav', 0, '2008'),
('olav_hell', 'Olav', 0, '2007'),
('olav_lien', 'Olav', 0, '2002'),
('ole_borg', 'Ole', 0, '2002'),
('reid_hamr', 'Reidun', 0, '2008'),
('rolf_wiik', 'Rolf', 0, '2002'),
('rune_haga', 'Rune', 0, '2005'),
('sara_okst', 'Sarah', 0, '2003'),
('silj_mykl', 'Silje', 0, '2007'),
('sive_nord', 'Sivert', 0, '2009'),
('solv_solb', 'Solveig', 0, '2004'),
('stia_andr', 'Stian', 0, '2004'),
('stia_haug', 'Stian', 0, '2002'),
('stia_henr', 'Stian', 0, '2001'),
('terj_mort', 'Terje', 0, '2003'),
('thom_inge', 'Thomas', 0, '2006'),
('tom_bråt', 'Tom', 0, '2008'),
('tom_bøe', 'Tom', 0, '2008'),
('tom_jako', 'Tom', 0, '2002'),
('tore_gulb', 'Tore', 0, '2005'),
('tore_svee', 'Tore', 0, '2001'),
('tor_dale', 'Tor', 0, '2005'),
('tove_moe', 'Tove', 0, '2002'),
('trin_kals', 'Trine', 0, '2009'),
('tron_kris', 'Trond', 0, '2006'),
('tron_moen', 'Trond', 0, '2004'),
('øyst_aase', 'Øystein', 0, '2007'),
('øyst_lore', 'Øystein', 0, '2004'),
('øyst_sæth', 'Øystein', 0, '2000'),
('øyvi_hell', 'Øyvind', 0, '2000'),
('øyvi_jens', 'Øyvind', 0, '1999'),
('øyvi_kvam', 'Øyvind', 0, '2000'),
('øyvi_vike', 'Øyvind', 0, '2004');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`clubId`),
  ADD UNIQUE KEY `clubId` (`clubId`);

--
-- Indexes for table `cskier`
--
ALTER TABLE `cskier`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `distance`
--
ALTER TABLE `distance`
  ADD PRIMARY KEY (`userName`,`fallYear`),
  ADD KEY `fallYear` (`fallYear`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`fallYear`),
  ADD UNIQUE KEY `fallYear` (`fallYear`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`),
  ADD UNIQUE KEY `userName` (`userName`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `distance`
--
ALTER TABLE `distance`
  ADD CONSTRAINT `distance_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `skier` (`userName`),
  ADD CONSTRAINT `distance_ibfk_2` FOREIGN KEY (`fallYear`) REFERENCES `season` (`fallYear`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
